# Solvemate Coding Challenge

This project consists on 3 different Docker containers:
- One for a relational database (rel_db)
- One for the Django app (web)


# Setting the project up

In order to configure everything, there's a little script in `bin/setup` which will run the migrations and load some fixtures. Simply execute:
```
$ ./bin/setup
```
and follow the instructions.

# Start the containers
```
$ docker-compose up
```

# Start the web server
```
$ ./bin/runserver
```

# Load the address dataset
```
docker-compose exec web ./manage.py load_dataset --path=../Berlin_Adressen.csv
```

# Endpoints

```
# Building count per zip code
http://localhost:8000/api/v1/building/count/
http://localhost:8000/api/v1/building/count/?zip_codes=10249
http://localhost:8000/api/v1/building/count/?zip_codes=10249,10245,10248

# Building year distribution per zip code
http://localhost:8000/api/v1/building/years/zip-code/
http://localhost:8000/api/v1/building/years/zip-code/?zip_codes=10249
http://localhost:8000/api/v1/building/years/zip-code/?zip_codes=10249,10245,10248
http://localhost:8000/api/v1/building/years/?format=html&zip_codes=10115,10249,10245

# Buildings per year
http://localhost:8000/api/v1/building/years/
http://localhost:8000/api/v1/building/years/?format=html
```

Each endpoint includes a browsable API if no format is specified, pure json if `format=json` is specified, and an HTML view if `format=html` is included as a parameter.


# -*- coding: utf-8 -*-
from django.urls import path

from webclient import views

app_name = 'webclient'
urlpatterns = [

    path(
        '',
        views.DatasetLoader.as_view(),
        name='dataset-loader'
    ),
]

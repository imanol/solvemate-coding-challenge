from django.apps import AppConfig
from django.conf import settings


class WebclientConfig(AppConfig):
    name = 'webclient'

    def ready(self):
        """Load the active dataset once the app is ready."""
        if not settings.TEST:
            try:
                from .models import Dataset
                dataset = Dataset.objects.load_active_dataset()
                setattr(settings, "DATASET", dataset)
            except Exception:
                # if the migrations are not executed, this will fail
                print(
                    u'Run \'./manage.py migrate\' command before using the app'
                )

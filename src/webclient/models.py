# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models

import pandas as pd


class DatasetManager(models.Manager):
    """Custom manager for the Datasets."""

    def get_active_dataset(self):
        """Return the active dataset."""
        return Dataset.objects.filter(is_active=True).first()

    def load_active_dataset(self):
        """Load the default dataset and make it available site-wide."""
        dataset = self.get_active_dataset()
        if dataset:
            return dataset.load_dataset()

    def get_info(self):
        """Get information about the active dataset."""
        dataset = self.get_active_dataset()
        if dataset:
            return {
                'name': dataset.name,
                'path': dataset.dataset_file.path,
                'size': len(settings.DATASET)
            }


class Dataset(models.Model):
    """Model for handling datasets."""

    name = models.CharField(max_length=100, blank=False, null=False)
    # file will be uploaded to MEDIA_ROOT/datasets
    dataset_file = models.FileField(upload_to='datasets/')
    is_active = models.BooleanField(default=False)

    objects = DatasetManager()

    def select_dataset(self):
        """Set this dataset as the active one, deactivate the others."""
        Dataset.objects.exclude(id=self.id).update(is_active=False)
        self.is_active = True
        self.save()
        return self.load_dataset()

    def load_dataset(self):
        """Load the dataset into memory."""
        print("Loading dataset {}".format(self.name))
        dataset = pd.read_csv(self.dataset_file.path, low_memory=False)
        print('Dataset loaded: {} entries'.format(len(dataset)))
        return dataset

# -*- coding: utf-8 -*-
from django.contrib import admin

from webclient.models import Dataset


@admin.register(Dataset)
class DatasetAdmin(admin.ModelAdmin):
    """Admin interface for the model Dataset."""

    list_display = (
        'id',
        'name',
        'dataset_file',
        'is_active',
    )

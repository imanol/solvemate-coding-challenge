# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings

from webclient.models import Dataset


class DatasetForm(forms.ModelForm):
    """Form for uploading datasets."""

    # override the widget of the attributes to add some extra features
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )
    dataset_file = forms.FileField(
        widget=forms.FileInput(
            attrs={'class': 'form-control'}
        )
    )
    load = forms.BooleanField(
        initial=True,
        widget=forms.CheckboxInput(
            attrs={'class': 'form-control'}
        )
    )

    class Meta:
        model = Dataset
        fields = ['name', 'dataset_file']

    def save(self):
        """Override the save method to activate and load the dataset."""
        dataset = super(DatasetForm, self).save()
        if self.cleaned_data['load'] or settings.DATASET is None:
            return dataset.select_dataset()

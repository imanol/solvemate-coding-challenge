# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View

from webclient.forms import DatasetForm
from webclient.models import Dataset


class DatasetLoader(View):
    """Load the dataset."""

    form_class = DatasetForm
    template_name = "webclient/upload_dataset.html"

    @method_decorator(login_required)
    def get(self, request):
        """Display the form for loading the dataset."""
        context = {}
        context['form'] = self.form_class()
        context['dataset'] = Dataset.objects.get_info()
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """Process the uploaded dataset."""
        context = {}
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            context['success_message'] = 'Dataset uploaded'
            form = self.form_class()  # reset the form

        context['form'] = form
        context['dataset'] = Dataset.objects.get_info()
        return render(request, self.template_name, context)

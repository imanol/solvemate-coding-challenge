# -*- coding: utf-8 -*-
from rest_framework.renderers import TemplateHTMLRenderer


class MyTemplateHTMLRenderer(TemplateHTMLRenderer):
    """Custom HTML renderer to allow compatiblity with other renderers.

    Avoid `context must be a dict rather than ReturnList`.
    """

    def get_template_context(self, data, renderer_context):
        """Get the template context."""
        response = renderer_context['response']
        if response.exception:
            data['status_code'] = response.status_code
        return {'data': data}

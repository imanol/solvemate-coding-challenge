# -*- coding: utf-8 -*-
from django.urls import path

from api import views

app_name = 'api'
urlpatterns = [

    path(
        'v1/building/count/',
        views.BuildingCountPerZipCode.as_view(),
        name='building-count-per-zip-code'
    ),
    path(
        'v1/building/years/',
        views.BuildingYearsDistribution.as_view(),
        name='building-years-distribution'
    ),
    path(
        'v1/building/years/zip-code/',
        views.BuildingYearsByPostalCodeDistribution.as_view(),
        name='building-years-by-plz-distribution'
    ),
]

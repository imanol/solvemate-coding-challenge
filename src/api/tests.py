# -*- coding: utf-8 -*-
from unittest import skip

from django.test import TestCase, override_settings
from django.urls import reverse
import numpy as np
import pandas as pd
from rest_framework import status
from rest_framework.test import APIClient


DATASET_YEARS = [
    1960, 1975, 1986, 1995, 1999, 2000, 2001, 2008, 2011, 2015, 2016, 2018]
DATASET_ZIP_CODES = [10245, 10248, 10249, 12455, 13107, 13333]

DATASET_ZIP_CODES_COUNT = {
    10245: 4,
    10248: 2,
    10249: 6,
    12455: 1,
    13107: 1,
    13333: 1,
}

DATASET_10249_DISTRIBUTION = {
    1986: 1,
    1999: 1,
    2011: 1,
    2018: 3,
}


def create_dataset():
    """Create random dataframe."""
    # TODO: randomize this data
    data = np.array([
        ['', 'PLZ', 'STR_DATUM', 'ADRESSID'],
        [0, 10245, '1960-01-01T00:00:0', 1],
        [1, 10245, '1960-02-01T00:00:0', 2],
        [2, 10245, '2008-03-01T00:00:0', 3],
        [3, 10245, '2001-04-01T00:00:0', 4],
        [4, 10248, '1975-05-01T00:00:0', 5],
        [5, 10248, '2015-06-01T00:00:0', 6],
        [6, 10249, '2018-07-01T00:00:0', 7],
        [7, 10249, '2018-08-01T00:00:0', 8],
        [8, 10249, '2018-08-01T00:00:0', 9],
        [9, 10249, '1986-03-01T00:00:0', 10],
        [10, 10249, '1999-02-01T00:00:0', 11],
        [11, 10249, '2011-04-01T00:00:0', 12],
        [12, 12455, '2016-09-01T00:00:0', 13],
        [13, 13107, '1995-10-01T00:00:0', 14],
        [14, 13333, '2000-11-01T00:00:0', 15],
    ])

    df = pd.DataFrame(
        data=data[1:, 1:],
        index=data[1:, 0],
        columns=data[0, 1:],
    )
    # cast the PLZ column to integer
    df.PLZ = df.PLZ.astype(int)

    return df


class BuildingCountPerZipCodeTest(TestCase):
    """Tests for `BuildingCountPerZipCode` endpoint."""

    def setUp(self):
        """Prepare the test environment."""
        self.client = APIClient()
        self.url = reverse('api:building-count-per-zip-code')

    @override_settings(DATASET=create_dataset())
    def test_get_building_count_per_zip_code_all_zip_codes(self):
        """Test getting the building count per zip code (all zip codes)."""
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), len(DATASET_ZIP_CODES))
        for result in results:
            self.assertTrue(result['zip_code'] in DATASET_ZIP_CODES)
            self.assertEquals(
                result['count'],
                DATASET_ZIP_CODES_COUNT.get(result['zip_code'])
            )

    @override_settings(DATASET=create_dataset())
    def test_get_building_count_per_zip_code_filter_one_zip_code(self):
        """Test getting the building count per zip code (filter just one)."""
        selected_zip_code = 10249
        response = self.client.get(
            self.url, {'zip_codes': selected_zip_code}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), 1)
        self.assertTrue(results[0]['zip_code'], selected_zip_code)
        self.assertEquals(
            results[0]['count'],
            DATASET_ZIP_CODES_COUNT.get(results[0]['zip_code'])
        )

    @override_settings(DATASET=create_dataset())
    def test_get_building_count_per_zip_code_filter_multiple_zip_codes(self):
        """Test getting the building count per zip code (filter multiple)."""
        filter_zip_codes = '10248,10249'
        response = self.client.get(
            self.url,
            {'zip_codes': filter_zip_codes},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), len(filter_zip_codes.split(',')))
        self.assertTrue(
            results[0]['zip_code'] in map(int, filter_zip_codes.split(',')))
        self.assertEquals(
            results[0]['count'],
            DATASET_ZIP_CODES_COUNT.get(results[0]['zip_code'])
        )

    @override_settings(DATASET=create_dataset())
    def test_get_building_count_per_zip_code_filter_invalid_zip_code(self):
        """Test getting the building count per zip code (filter invalid)."""
        response = self.client.get(
            self.url, {'zip_codes': "foobar"},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), 0)

    @override_settings(DATASET=create_dataset())
    def test_get_building_count_per_zip_code_filter_nonexistent_zip_code(self):
        """Test getting the building count per zip code (non existent PLZ)."""
        nonexistent_zip_code = 100
        self.assertTrue(nonexistent_zip_code not in DATASET_ZIP_CODES)
        response = self.client.get(
            self.url, {'zip_codes': nonexistent_zip_code},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), 0)


class BuildingYearsByPostalCodeistributionTest(TestCase):
    """Test the Year Distribution by postal code."""

    def setUp(self):
        """Prepare the test environment."""
        self.client = APIClient()
        self.url = reverse('api:building-years-by-plz-distribution')

    @override_settings(DATASET=create_dataset())
    def test_get_year_distribution_all_zip_codes(self):
        """Test building's year distribution per zip code (all zip codes)."""
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), len(DATASET_ZIP_CODES))
        for result in results:
            zip_code = result['zip_code']
            distribution = result['distribution']
            self.assertTrue(zip_code in DATASET_ZIP_CODES)
            for elem in distribution:
                self.assertTrue(elem['year'] in DATASET_YEARS)

            if zip_code == 10249:
                for elem in distribution:
                    self.assertEqual(
                        DATASET_10249_DISTRIBUTION.get(elem['year']),
                        elem['count'],
                    )

    @override_settings(DATASET=create_dataset())
    def test_get_year_distribution_one_zip_codes(self):
        """Test building's year distribution per zip code (one zip code)."""
        selected_zip_code = 10249
        response = self.client.get(
            self.url, {'zip_codes': selected_zip_code}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']

        self.assertEqual(len(results), 1)
        result = results[0]
        zip_code = result['zip_code']
        distribution = result['distribution']
        self.assertEqual(zip_code, selected_zip_code)
        for elem in distribution:
            self.assertEqual(
                DATASET_10249_DISTRIBUTION.get(elem['year']), elem['count'])

    @override_settings(DATASET=create_dataset())
    def test_get_year_distribution_multiple_zip_codes(self):
        """Test building's year distribution per zip code (filter multiple)."""
        filter_zip_codes = '10248,10249'
        response = self.client.get(
            self.url, {'zip_codes': filter_zip_codes}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']

        self.assertEqual(len(results), len(filter_zip_codes.split(',')))
        for result in results:
            zip_code = result['zip_code']
            distribution = result['distribution']
            self.assertTrue(zip_code in DATASET_ZIP_CODES)
            for elem in distribution:
                if zip_code == 10249:
                    self.assertEqual(
                        DATASET_10249_DISTRIBUTION.get(elem['year']),
                        elem['count'],
                    )

    @override_settings(DATASET=create_dataset())
    def test_get_year_distribution_filter_invalid_zip_code(self):
        """Test building's year distribution per zip code (filter invalid)."""
        response = self.client.get(
            self.url, {'zip_codes': "foobar"},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), 0)

    @override_settings(DATASET=create_dataset())
    def test_get_year_distribution_filter_nonexistent_zip_code(self):
        """Test building's year distribution per zipcode (non existent PLZ)."""
        nonexistent_zip_code = 100
        self.assertTrue(nonexistent_zip_code not in DATASET_ZIP_CODES)
        response = self.client.get(
            self.url, {'zip_codes': nonexistent_zip_code},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), 0)


class BuildingYearsDistributionTest(TestCase):
    """Get the distribution of years in which buildings were added."""

    def setUp(self):
        """Prepare the test environment."""
        self.client = APIClient()
        self.url = reverse('api:building-years-distribution')

    @override_settings(DATASET=create_dataset())
    def test_get_year_distribution(self):
        """Test building's year distribution."""
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(results), len(DATASET_YEARS))

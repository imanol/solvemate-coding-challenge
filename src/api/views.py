# -*- coding: utf-8 -*-

from rest_framework import status, renderers
from rest_framework.response import Response
from rest_framework.views import APIView

from api.renderers import MyTemplateHTMLRenderer
from api import services as APIService

from api.services import get_count_by_zip_codes


class SolvemateAPIBaseAPIView(APIView):
    """Base APIView with common logic."""

    qs_method = None  # you must define it in the child class

    def parse_zip_codes(self, request):
        """Figure out which zip codes do we use in the query."""
        zip_codes = request.GET.get('zip_codes')
        parsed = zip_codes.split(',') if zip_codes else []
        return parsed

    def error_response(self, errors):
        """Format error response."""
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

    def success_response(self, data):
        """Format success response."""
        response_data = {}
        if isinstance(data, list):
            response_data['results'] = data
            response_data['results_count'] = len(data)
        elif isinstance(data, dict):
            response_data['result'] = data

        return Response(response_data, status=status.HTTP_200_OK)

    def get(self, request, format=None):
        """Get the number of buildings per zip code."""
        dataset = APIService.get_dataset()
        zip_codes = self.parse_zip_codes(request)
        data = self.qs_method(dataset, zip_codes)
        return self.success_response(data)


class BuildingCountPerZipCode(SolvemateAPIBaseAPIView):
    """Deliver the number of buildings per zip code.

    If `zip_codes` is not provided, it will query all the available zip codes
    in the dataset, otherwise, it will filter by the ones provided.

    Examples:
    * `/api/v1/building/count/`
    * `/api/v1/building/count/?zip_codes=10249`
    * `/api/v1/building/count/?zip_codes=10249,10245,10248`

    """

    qs_method = staticmethod(APIService.get_count_by_zip_codes)


class BuildingYearsByPostalCodeDistribution(SolvemateAPIBaseAPIView):
    """Get the distribution of years by PLZ in which buildings were added.

    Examples:
    * `/api/v1/building/years/zip-code/`
    * `/api/v1/building/years/zip-code/?zip_codes=10249`
    * `/api/v1/building/years/zip-code/?zip_codes=10249,10245,10248`

    """

    renderer_classes = (
        renderers.BrowsableAPIRenderer,
        renderers.JSONRenderer,
        MyTemplateHTMLRenderer,
    )
    template_name = 'api/distribution-plz.html'
    qs_method = staticmethod(APIService.get_distribution_per_year_by_zip_code)


class BuildingYearsDistribution(SolvemateAPIBaseAPIView):
    """Get the distribution of years in which buildings were added.

    This endpoint is outside of the scope of the challenge, and has been done
    as an experiment, that's why the zip_codes filtering is not implemented,
    it can filter just by one PLZ.

    Examples:
    * `/api/v1/building/years/`

    """
    renderer_classes = (
        renderers.BrowsableAPIRenderer,
        renderers.JSONRenderer,
        MyTemplateHTMLRenderer,
    )
    template_name = 'api/distribution.html'
    qs_method = staticmethod(APIService.get_distribution_per_year)

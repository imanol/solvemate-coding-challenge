# -*- coding: utf-8 -*-
"""Logic for dealing with Datasets."""
from django.conf import settings
import pandas as pd


def get_dataset():
    """Temporary data loader."""
    dataset = settings.DATASET
    # doing a `if dataset:` will be taken as a pandas operation that
    # will fail. Compare explicitly with `None`:
    if dataset is not None:
        return dataset.copy()  # MUTABLE OBJECTS ARE EVIL
    else:
        raise Exception('No dataset loaded')


def filter_out_empty_zip_codes(dataset, zip_codes):
    """Filter out the zip codes that have no entries in the dataset."""
    valid_zip_codes = []
    for zip_code in zip_codes:
        if len(dataset.query('PLZ in ({})'.format(zip_codes))):
            valid_zip_codes.append(zip_code)
    return valid_zip_codes


def get_count_by_zip_code(dataset, zip_code):
    """Get the number of entries in one PLZ."""
    len(dataset.loc[dataset.PLZ == zip_code])


def get_count_by_zip_codes(dataset, zip_codes=None):
    """Get the number of entries per PLZ."""
    results = []
    if not zip_codes:
        # by default offer all
        zip_codes = dataset.PLZ.unique()

    filtered = dataset.loc[dataset.PLZ.isin(zip_codes)]
    qs = filtered.groupby('PLZ').size().to_dict()
    # :(
    for elem in qs.items():
        results.append({'zip_code': elem[0], 'count': elem[1]})
    return results


def get_distribution_per_year_by_zip_code(dataset, zip_codes=None):
    """Get distribution of years in which buildings were added by PLZ."""
    # TODO: move this check/filtering to a method and get the filtered dataset
    if zip_codes:
        valid_zip_codes = filter_out_empty_zip_codes(dataset, zip_codes)
        if valid_zip_codes:
            dataset = dataset.loc[dataset['PLZ'].isin(valid_zip_codes)]
        else:
            return []

    # transform string column into a date column (and get the year)
    dataset.STR_DATUM = pd.to_datetime(dataset.STR_DATUM).dt.year
    # get the dataframe
    df = dataset.groupby(['PLZ', 'STR_DATUM']).ADRESSID.size().reset_index()
    # format the information that we need and format it into a dict
    # Important: this fails if the PLZ is not present in the dataframe
    # but at this point, the empty PLZs should have been filtered out
    df["TMP"] = df.reset_index().apply(
        lambda row: {
            'year': row.STR_DATUM,
            'count': row.ADRESSID
        },
        axis=1,
    )
    # group the resulting dataframe by PLZ and export the data into a list
    df2 = df.groupby("PLZ")[["PLZ", "TMP"]].apply(
        lambda row: {
            "zip_code": row.PLZ.unique()[0],  # row.PLZ.tolist()[0] also ok
            "distribution": row.TMP.tolist()
        }
    )
    return df2.tolist()


def get_distribution_per_year(dataset, zip_codes=None):
    """Get the count of buildings per year.

    Limitation: can filter just by one PLZ (this is an extra, out of the
    scope).
    """
    results = []
    # TODO: move this check/filtering to a method and get the filtered dataset
    if zip_codes:
        valid_zip_codes = filter_out_empty_zip_codes(dataset, zip_codes)
        if valid_zip_codes:
            dataset = dataset.loc[dataset['PLZ'].isin(valid_zip_codes)]
        else:
            return results

    # transform string column into a date column (and get the year)
    dataset.STR_DATUM = pd.to_datetime(dataset.STR_DATUM).dt.year
    # get the distribution
    qs = dataset.ADRESSID.groupby(dataset.STR_DATUM).size().to_dict()
    for elem in qs.items():
        results.append({'year': elem[0], 'count': elem[1]})
    return results
